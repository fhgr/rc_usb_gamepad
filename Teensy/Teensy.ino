/*
  You must select "Serial + Keyboard + Mouse + Joystick" from the "Tools > USB Type" menu
*/

#include "src/sbus/sbus.h"

bfs::SbusRx sbus_rx(&Serial1);
std::array<int16_t, bfs::SbusRx::NUM_CH()> sbus_data;
const uint8_t FAILSAFE_BUTTON_NUM = 32;
const uint8_t LOST_FRAME_BUTTON_NUM = 31;

// lower limit in us
const uint16_t UL = 170;

// higher limit in us
const uint16_t UH = 1816;

// linear scaling
unsigned int calculate_value_unsigned(int x) {
  return 21845 / 562 * x - 1638375 / 281;
}

// for buttons and switches use 33th percentile as threshold. 0-33% = false, 34-100% = true
const uint16_t SWITCH_THRESHOLD = UL + ((UH - UL) / 3);

void setup() {
  Joystick.useManualSend(true);
  sbus_rx.Begin();
}

void loop() {

  // check for a valid SBUS packet
  if (sbus_rx.Read()) {
    // Grab the received data
    sbus_data = sbus_rx.ch();

    // set failsafe and lost_frame at corresponding buttons
    Joystick.button(FAILSAFE_BUTTON_NUM, sbus_rx.failsafe());
    Joystick.button(LOST_FRAME_BUTTON_NUM, sbus_rx.lost_frame());

    if (sbus_rx.lost_frame() || sbus_rx.failsafe()) {
      // send current state of gamepad and return, no need to send other values
      Joystick.send_now();
      return;
    }

    // map received channels 1-11 to corresponding analog channels 1-11. Convert data from SBUS us to 0-65535
    for (int i = 0; i < 12; i++) {
      Joystick.analog16(i, calculate_value_unsigned(sbus_data[i]));
    }

    // map received channels 1-16 to corresponding digital / button channels 1-16 (button is offset by 1 therefore i + 1)
    for (int i = 0; i < bfs::SbusRx::NUM_CH(); i++) {
      Joystick.button(i + 1, sbus_data[i] > SWITCH_THRESHOLD);
    }

    Joystick.send_now();
  }
}

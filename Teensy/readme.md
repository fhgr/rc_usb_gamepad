# Teensy 3.x

# Installation

Um die Software zu kompilieren oder auf den Microcontroller zu laden werden zwei Installationen benötigt:
1. Arduino IDE (https://www.arduino.cc/en/software)
2. Teensyduino (https://www.pjrc.com/teensy/td_download.html)

Diese Firmware verwendet veränderte Core-Libraries von Teensy, um die default USB DeviceDescriptors zu ändern. Dies ist notwendig, um ein Gamepad mit mehr als 6 Achsen zu erstellen.

Dazu müssen die vier Files unter ``Teensy/src/gamepad`` nach ``C:\Program Files (x86)\Arduino\hardware\teensy\avr\cores\teensy3`` kopiert und ersetzt werden.

# Konfiguration IDE

In Arduino folgende Einstellungen übernehmen:

1. Tools > Board > Teensy 3.2 / 3.1
2. Tools > USB Type > Serial + Keyboard + Mouse + Joystick
3. Tools > Port > Port auswählen

# Funktionsweise

Über die USB Schnittstelle wird ein virtuelles Gamepad (Joystick) zur Verfügung gestellt. Das Gamepad hat 8 Achsen und 32 Buttons. Über SBUS werden insgesamt 12 Channels von den Empfänger eingelesen. Die ersten 8 Channels werden auf die 8 Achsen und die restlichen 4 Channels auf die 4 Buttons gemappt. Das Failsafe Bit wird über einen weiteren Button übermittelt. Weitere Informationen sind in der untenstehenden Tabelle ersichtlich.

Je nach Betriebssystem erhalten die Achsen unterschiedliche Namen im Treiber.

## Axis

| Channel | Axis-Id | Name | Output Value Range |
|--|--|--|--|
|1| 0 | ABS_X| 0 bis 65535|
|2| 1 | ABS_Y| 0 bis 65535|
|3| 2 | ABS_Z| 0 bis 65535|
|4| 3 | ABS_RX| 0 bis 65535|
|5| 4 | ABS_RY| 0 bis 65535|
|6| 5 | ABS_RZ| 0 bis 65535|
|7| 6 | ABS_THROTTLE| 0 bis 65535|
|8| 7 | ABS_RUDDER | 0 bis 65535|
|9| 8 | ABS_WHEEL | 0 bis 65535|
|10| 9 | Hat0X | 0 bis 65535|
|11| 10 | (null) | 0 bis 65535|

## Buttons
| Channel | Button-Id | Output Value Range |
|--|--|--|
|1| 0 | 0 bis 1|
|2| 1 | 0 bis 1|
|3| 2 | 0 bis 1|
|4| 3 | 0 bis 1|
|5| 4 | 0 bis 1|
|6| 5 | 0 bis 1|
|7| 6 | 0 bis 1|
|8| 7 | 0 bis 1|
|9| 8 | 0 bis 1|
|10| 9 | 0 bis 1|
|11| 10 | 0 bis 1|
|12| 11 | 0 bis 1|
|13| 12 | 0 bis 1|
|14| 13 | 0 bis 1|
|15| 14 | 0 bis 1|
|16| 15 | 0 bis 1|
|N/A| 16 | 0 bis 1|
|N/A| 17 | 0 bis 1|
|N/A| 18 | 0 bis 1|
|N/A| 19 | 0 bis 1|
|N/A| 20 | 0 bis 1|
|N/A| 21 | 0 bis 1|
|N/A| 22 | 0 bis 1|
|N/A| 23 | 0 bis 1|
|N/A| 24 | 0 bis 1|
|N/A| 25 | 0 bis 1|
|N/A| 26 | 0 bis 1|
|N/A| 27 | 0 bis 1|
|N/A| 28 | 0 bis 1|
|N/A| 29 | 0 bis 1|
|Lost Frame| 30 | 0 bis 1|
|Failsafe| 31 | 0 bis 1|


# Verdrahtung

![TeensyRx.png](./TeensyRx.png)


# DeviceDescriptor konfigurieren

Von PJRC (Teensy Hersteller) gibt es kaum Informationen diesbezüglich. Alle Informationen wurden durch ausprobieren ermittelt und sind somit nicht garantiert korrekt. 

Um Änderungen am USB Descriptor zu machen werden grundsätzlich folgende drei Files angepasst:

1. usb_desc.h
2. usb_desc.c
3. usb_joystick.h


## usb_desc.h

In diesem File ist lediglich die Zeile 335 (``#define JOYSTICK_SIZE 50``) interessant. ``JOYSTICK_SIZE`` definiert die Anzahl der Bytes die übermittelt werden.

Diese kann folgendermassen berechnet werden:


``JOYSTICK_SIZE = ceil((Anzahl Buttons) / 8) + ceil((Anzahl Analog) * (Analog Bit Size / 8)) + ceil((Anzahl HAT) / 4)``

Achtung: Ändert man ``JOYSTICK_SIZE`` zu einem Wert der nicht 12, 50 oder 64 ist muss man ggf. alle oben erwähnten Files anpassen da diese ``#if JOYSTICK_SIZE == 12`` enthalten. Am besten einfach alle Files danach durchsuchen und den Code entsprechend anpassen.

## usb_desc.c

Dieses File definiert den USB Report Descriptor. Relevant dafür ist das Array ``static uint8_t joystick_report_desc[]``.

Anzahl Buttons werden definiert durch ``0x95, 0x20, // Report Count (32)`` in diesem Fall 32 Buttons (Je 1 Bit)

Anzahl Achsen werden definiert durch ``0x95, 23, // Report Count (23)`` hier 23 Achsen.

Auflösung der Achsen (Bit Size) wird definiert durch ``0x15, 0x00, // Logical Minimum (0)`` und ``0x27, 0xFF, 0xFF, 0, 0, // Logical Maximum (65535)``

Die Art der Achsen wird definiert durch ``0x09, 0x30, // Usage (X)`` hier ist ``0x30`` = X Achse. (Siehe https://www.freebsddiary.org/APC/usb_hid_usages.php)

Entscheidend ist die Reihenfolge. Das erste ``Usage`` definiert die erste Achse usw. Je nach Betriebssystem werden keine doppelten Einträge (z.B. mehrere X Achsen) unterstüzt. Am besten experimentell ermitteln.

## usb_joystick.h

Dieses File enthält die Klasse usb_joystick_class, welche im Arduino IDE unter dem Namen "Joystick" verfügbar ist (z.B. ``Joystick.button(1, true)``).

Hauptzweck dieser Klasse ist das Abfüllen eines Arrays anhand der an die Klasse übermittelten Werte für Achsen und Buttons. Dieses Array wird anschliessend über USB versendet. Leider ist diese Klasse nicht wirklich generisch aufgebaut und so müssen jedesmal Änderungen vorgenommen werden, wenn sich die Anzahl der Achsen oder Buttons ändern. Relevant dafür sind die Methoden ``void analog16(unsigned int num, unsigned int value) `` und `` void button(unsigned int num, bool val) ``.


Das 32 Bit Array wird in einer Bestimmten Reihenfolge abgefüllt.

1. Buttons
2. Achsen
3. HAT

Es ist zwar ein 32 Bit Array, wird teilweise aber trotzdem pro Byte abgefüllt.

Haben wir beispielsweise 32 Buttons und 23 Achsen (16 Bit), so ist das Array (32 Bit / 8) + 23 * (16 Bit / 8) = 50 Byte gross.

In Index 0 werden die Buttons abgespeichert.

Ab Index 1 befinden sich die Werte für die Achsen. Achtung: Haben wir z.B. 64 Buttons, so würden die Achsen erst bei Index 2 beginnen! Da es 2*32 Bit braucht um die Buttons zu speichern.

Der Index, wo die Achsen anfangen, muss im File definiert werden. Dies wird unter ``uint16_t *p = (uint16_t *)(&usb_joystick_data[1]);`` gemacht. In diesem Fall ist es Index 1.

Verwendet man eine kleinere Achsenauflösung z.B. 8 Bit, müsste diese Zeile entsprechend angepasst werden. ``uint8_t *p = (uint8_t *)(&usb_joystick_data[1]);``

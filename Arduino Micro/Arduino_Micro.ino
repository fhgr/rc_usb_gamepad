#include "src/Joystick/Joystick.h"
#include "src/RcTrainer/RcTrainer.h"

// "Joystick" type is GAMEPAD. Any connected device will load a driver for a Gamepad instead
// If you want to use multiple devices (gamepads) on the same computer, use different instances of this object with different JOYSTICK_REPORT_ID's
Joystick_ Gamepad1(JOYSTICK_DEFAULT_REPORT_ID, JOYSTICK_TYPE_GAMEPAD,
  8, 0,                  // Button Count, Hat Switch Count
  true, true, true,      // X, Y and Z Axis enabled
  true, true, true,      // Rx, Ry and Rz enabled
  true, true,            // Rudder and throttle enabled
  false, false, false);  // No Accelerator, brake or steering enabled

Joystick_ Gamepad2(JOYSTICK_DEFAULT_REPORT_ID + 1, JOYSTICK_TYPE_GAMEPAD,
  8, 0,                  // Button Count, Hat Switch Count
  true, true, true,      // X, Y and Z Axis enabled
  true, true, true,      // Rx, Ry and Rz enabled
  true, true,            // Rudder and throttle enabled
  false, false, false);  // No Accelerator, brake or steering enabled

const uint8_t CHANNEL_01 = 0;
const uint8_t CHANNEL_02 = 1;
const uint8_t CHANNEL_03 = 2;
const uint8_t CHANNEL_04 = 3;
const uint8_t CHANNEL_05 = 4;
const uint8_t CHANNEL_06 = 5;
const uint8_t CHANNEL_07 = 6;
const uint8_t CHANNEL_08 = 7;
const uint8_t CHANNEL_09 = 8;
const uint8_t CHANNEL_10 = 9;
const uint8_t CHANNEL_11 = 10;
const uint8_t CHANNEL_12 = 11;
const uint8_t CHANNEL_13 = 12;
const uint8_t CHANNEL_14 = 13;
const uint8_t CHANNEL_15 = 14;
const uint8_t CHANNEL_16 = 15;

// lower limit in us
const uint16_t UL = 1000;

// higher limit in us
const uint16_t UH = 2000;

// for buttons and switches use 33th percentile as threshold. 0-33% = false, 34-100% = true
const uint16_t SWITCH_THRESHOLD = UL + ((UH - UL) / 3);

RcTrainer tx(1);

void setup() { 

  // Gamepad 1
  Gamepad1.begin(true);
  
  Gamepad1.setXAxisRange(UL, UH);
  Gamepad1.setYAxisRange(UL, UH);
  Gamepad1.setZAxisRange(UL, UH);
  
  Gamepad1.setRxAxisRange(UL, UH);
  Gamepad1.setRyAxisRange(UL, UH);
  Gamepad1.setRzAxisRange(UL, UH);

  Gamepad1.setRudderRange(UL, UH);
  Gamepad1.setThrottleRange(UL, UH);

  // Gamepad 2
  Gamepad2.begin(true);
  
  Gamepad2.setXAxisRange(UL, UH);
  Gamepad2.setYAxisRange(UL, UH);
  Gamepad2.setZAxisRange(UL, UH);
  
  Gamepad2.setRxAxisRange(UL, UH);
  Gamepad2.setRyAxisRange(UL, UH);
  Gamepad2.setRzAxisRange(UL, UH);

  Gamepad2.setRudderRange(UL, UH);
  Gamepad2.setThrottleRange(UL, UH);
  
}

void loop() {

  // read ppm raw values (unit in us)
  uint16_t ch01 = tx.getChannelRaw(CHANNEL_01);
  uint16_t ch02 = tx.getChannelRaw(CHANNEL_02);
  uint16_t ch03 = tx.getChannelRaw(CHANNEL_03);
  uint16_t ch04 = tx.getChannelRaw(CHANNEL_04);
  uint16_t ch05 = tx.getChannelRaw(CHANNEL_05);
  uint16_t ch06 = tx.getChannelRaw(CHANNEL_06);
  uint16_t ch07 = tx.getChannelRaw(CHANNEL_07);
  uint16_t ch08 = tx.getChannelRaw(CHANNEL_08);
  
  uint16_t ch09 = tx.getChannelRaw(CHANNEL_09);
  uint16_t ch10 = tx.getChannelRaw(CHANNEL_10);
  uint16_t ch11 = tx.getChannelRaw(CHANNEL_11);
  uint16_t ch12 = tx.getChannelRaw(CHANNEL_12);
  uint16_t ch13 = tx.getChannelRaw(CHANNEL_13);
  uint16_t ch14 = tx.getChannelRaw(CHANNEL_14);
  uint16_t ch15 = tx.getChannelRaw(CHANNEL_15);
  uint16_t ch16 = tx.getChannelRaw(CHANNEL_16);

  // Gamepad 1, write analog values
  Gamepad1.setXAxis(ch01);
  Gamepad1.setYAxis(ch02);
  Gamepad1.setZAxis(ch03);
  
  Gamepad1.setRxAxis(ch04);
  Gamepad1.setRyAxis(ch05);
  Gamepad1.setRzAxis(ch06);

  Gamepad1.setRudder(ch07);
  Gamepad1.setThrottle(ch08);

  // Gamepad 2, write analog values
  Gamepad2.setXAxis(ch09);
  Gamepad2.setYAxis(ch10);
  Gamepad2.setZAxis(ch11);
  
  Gamepad2.setRxAxis(ch12);
  Gamepad2.setRyAxis(ch13);
  Gamepad2.setRzAxis(ch14);

  Gamepad2.setRudder(ch15);
  Gamepad2.setThrottle(ch16);

  // write digital values (switches / buttons). First parameter is the button id, second whether pressed or not
  // Gamepad 1
  Gamepad1.setButton(0, ch01 > SWITCH_THRESHOLD);
  Gamepad1.setButton(1, ch02 > SWITCH_THRESHOLD);
  Gamepad1.setButton(2, ch03 > SWITCH_THRESHOLD);
  Gamepad1.setButton(3, ch04 > SWITCH_THRESHOLD);
  Gamepad1.setButton(4, ch05 > SWITCH_THRESHOLD);
  Gamepad1.setButton(5, ch06 > SWITCH_THRESHOLD);
  Gamepad1.setButton(6, ch07 > SWITCH_THRESHOLD);
  Gamepad1.setButton(7, ch08 > SWITCH_THRESHOLD);

  // Gamepad 2
  Gamepad2.setButton(0, ch09 > SWITCH_THRESHOLD);
  Gamepad2.setButton(1, ch10 > SWITCH_THRESHOLD);
  Gamepad2.setButton(2, ch11 > SWITCH_THRESHOLD);
  Gamepad2.setButton(3, ch12 > SWITCH_THRESHOLD);
  Gamepad2.setButton(4, ch13 > SWITCH_THRESHOLD);
  Gamepad2.setButton(5, ch14 > SWITCH_THRESHOLD);
  Gamepad2.setButton(6, ch15 > SWITCH_THRESHOLD);
  Gamepad2.setButton(7, ch16 > SWITCH_THRESHOLD);
  
}

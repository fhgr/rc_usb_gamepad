# Micro Receiver USB Gamepad

**Micro Receiver USB Gamepad** ist eine Software um Gamepads über USB zu simulieren. 

## Funktionsweise
Die Software liest insgesamt 16 Channels von einem Empfänger (hier TBS Crossfire) über ein PPM Signal aus. Jeweils Kanäle 1-8 sowie 9-16 werden auf je einem virtuellen Gamepad über USB übertragen.  Alle Channels sind doppelt belegt: Analog und digital. So wird den Endusern überlassen, wie sie die Kanäle in der Empfangssoftware konfigurieren. Details dazu im Kapitel **Mappings**.

# Output - USB Gamepad

Wird der Microcontroller, mit dieser Software programmiert, über USB an einem PC verbunden, werden insgesamt zwei Gamepads als HID zur Verfügung gestellt. Dabei werden Standardtreiber verwendet, es ist somit nicht notwendig zusätzliche Treiber zu installieren. Die virtuellen Gamepads wurden mit der [ArduinoJoystickLibrary](https://github.com/MHeironimus/ArduinoJoystickLibrary) implementiert.

## Mappings

| Channel | Output digital (0-1) | Output analog (-32768 bis 32767) |  Gamepad# |
|--|--|--|--|
|1| BTN_SOUTH| ABS_X|  1 |
|2| BTN_EAST |  ABS_Y |1|
|3| BTN_C| ABS_Z|1|
|4| BTN_NORTH| ABS_RX|1|
|5| BTN_WEST|  ABS_RY|1|
|6| BTN_Z|  ABS_RZ|1|
|7| BTN_TL| ABS_RUDDER|1|
|8| BTN_TR|  ABS_THROTTLE|1|
|9| BTN_SOUTH| ABS_X|  2 |
|10| BTN_EAST |  ABS_Y |2|
|11| BTN_C| ABS_Z|2|
|12| BTN_NORTH| ABS_RX|2|
|13| BTN_WEST|  ABS_RY|2|
|14| BTN_Z|  ABS_RZ|2|
|15| BTN_TL| ABS_RUDDER|2|
|16| BTN_TR|  ABS_THROTTLE|2|


# Input - PPM

Aktuell unterstützt diese Software bis zu 16 Input-Channels. Die Inputs werden über das [PPM](https://de.wikipedia.org/wiki/Puls-Pausen-Modulation) Signal eingelesen. Die dafür verwendete Library ist [RcTrainer](http://www.airspayce.com/mikem/arduino/RcTrainer/).

# Hardware
Diese Software unterstützt folgende Boards:

 - **Arduino Micro (Pro)**
 - Arduino Leonardo*
 - Arduino Due*

*Pins müssen ggf. umkonfiguriert werden.

## Schema

![Verdrahtung](https://gitlab.com/armasuisse_fhgr/ugv-boarai/rc_usb_gamepad/-/raw/main/Arduino%20Micro/verdrahtung.png)

# Kompilieren/Upload

 1. Repository clonen
 2. Falls die RcTrainer oder Joystick Library über die Arduino IDE (Library Manager) installiert wurde. Diese entfernen, da die beiden Libs im Repository bereits vorhanden sind.
 3. Arduino IDE: Tools -> Board -> Arduino Micro auswählen
 4. Arduino IDE: Tools -> Programmer -> Arduino as ISP  (ATmega32U4) auswählen
 5. Arduino an PC anschliessen
 6. Upload klicken 

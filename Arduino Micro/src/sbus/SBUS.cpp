#include "sbus.h"
#include <Arduino.h>

void SbusRx::Begin()
{

	uart_->begin(BAUD_, SERIAL_8E2);
	uart_->flush();
}

bool SbusRx::Read()
{
	/* Read through all available packets to get the newest */
	new_data_ = false;
	do
	{
		if (Parse())
		{
			new_data_ = true;
		}
	} while (uart_->available());
	/* Parse new data, if available */
	if (new_data_)
	{
		/* Grab the channel data */
		ch_[0] = buf_[1] | buf_[2] << 8 & 0x07FF;
		ch_[1] = buf_[2] >> 3 | buf_[3] << 5 & 0x07FF;
		ch_[2] = buf_[3] >> 6 | buf_[4] << 2 | buf_[5] << 10 & 0x07FF;
		ch_[3] = buf_[5] >> 1 | buf_[6] << 7 & 0x07FF;
		ch_[4] = buf_[6] >> 4 | buf_[7] << 4 & 0x07FF;
		ch_[5] = buf_[7] >> 7 | buf_[8] << 1 | buf_[9] << 9 & 0x07FF;
		ch_[6] = buf_[9] >> 2 | buf_[10] << 6 & 0x07FF;
		ch_[7] = buf_[10] >> 5 | buf_[11] << 3 & 0x07FF;
		ch_[8] = buf_[12] | buf_[13] << 8 & 0x07FF;
		ch_[9] = buf_[13] >> 3 | buf_[14] << 5 & 0x07FF;
		ch_[10] = buf_[14] >> 6 | buf_[15] << 2 | buf_[16] << 10 & 0x07FF;
		ch_[11] = buf_[16] >> 1 | buf_[17] << 7 & 0x07FF;
		ch_[12] = buf_[17] >> 4 | buf_[18] << 4 & 0x07FF;
		ch_[13] = buf_[18] >> 7 | buf_[19] << 1 | buf_[20] << 9 & 0x07FF;
		ch_[14] = buf_[20] >> 2 | buf_[21] << 6 & 0x07FF;
		ch_[15] = buf_[21] >> 5 | buf_[22] << 3 & 0x07FF;
		/* CH 17 */
		ch17_ = buf_[23] & CH17_MASK_;
		/* CH 18 */
		ch18_ = buf_[23] & CH18_MASK_;
		/* Grab the lost frame */
		lost_frame_ = buf_[23] & LOST_FRAME_MASK_;
		/* Grab the failsafe */
		failsafe_ = buf_[23] & FAILSAFE_MASK_;
	}
	return new_data_;
}

int16_t SbusRx::GetChannelValue(int channel)
{
	return ch_[channel];
}

bool SbusRx::Parse()
{
	/* Parse messages */
	while (uart_->available())
	{
		cur_byte_ = uart_->read();
		if (state_ == 0)
		{
			if ((cur_byte_ == HEADER_) && ((prev_byte_ == FOOTER_) ||
										   ((prev_byte_ & 0x0F) == FOOTER2_)))
			{
				buf_[state_++] = cur_byte_;
			}
			else
			{
				state_ = 0;
			}
		}
		else
		{
			if (state_ < BUF_LEN_)
			{
				buf_[state_++] = cur_byte_;
			}
			else
			{
				state_ = 0;
				if ((buf_[BUF_LEN_ - 1] == FOOTER_) ||
					((buf_[BUF_LEN_ - 1] & 0x0F) == FOOTER2_))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		prev_byte_ = cur_byte_;
	}
	return false;
}

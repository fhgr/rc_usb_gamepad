#ifndef SRC_SBUS_H_
#define SRC_SBUS_H_

#include <Arduino.h>

class SbusRx
{
private:
	/* Communication */
	HardwareSerial *uart_;
	static constexpr uint32_t BAUD_ = 100000;

	/* Message len */
	static constexpr int8_t BUF_LEN_ = 25;

	/* SBUS message defs */
	static constexpr int8_t NUM_SBUS_CH_ = 16;
	static constexpr uint8_t HEADER_ = 0x0F;
	static constexpr uint8_t FOOTER_ = 0x00;
	static constexpr uint8_t FOOTER2_ = 0x04;
	static constexpr uint8_t CH17_MASK_ = 0x01;
	static constexpr uint8_t CH18_MASK_ = 0x02;
	static constexpr uint8_t LOST_FRAME_MASK_ = 0x04;
	static constexpr uint8_t FAILSAFE_MASK_ = 0x08;
	
	/* Parsing state tracking */
	int8_t state_ = 0;
	uint8_t prev_byte_ = FOOTER_;
	uint8_t cur_byte_;

	/* Buffer for storing messages */
	uint8_t buf_[BUF_LEN_];
	
	/* Data */
	bool new_data_;
	int16_t ch_[NUM_SBUS_CH_];
	bool failsafe_ = false, lost_frame_ = false, ch17_ = false, ch18_ = false;
	bool Parse();

public:
	explicit SbusRx(HardwareSerial *bus) : uart_(bus) {}
	void Begin();
	bool Read();
	int16_t GetChannelValue(int channel);
	static constexpr int8_t NUM_CH() { return NUM_SBUS_CH_; }
	inline bool failsafe() const { return failsafe_; }
	inline bool lost_frame() const { return lost_frame_; }
	inline bool ch17() const { return ch17_; }
	inline bool ch18() const { return ch18_; }
};

#endif // SRC_SBUS_H_